package com.mogujie.hive.jdbcDemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


 
public class HiveJDBClient {
	
	/**Hive的驱动字符串*/
	private static String driver="org.apache.hive.jdbc.HiveDriver";
	
	
	
	public static void main(String[] args) throws Exception{
		//加载Hive驱动
		Class.forName(driver);
		//获取hive2的jdbc连接，注意默认的数据库是default
		Connection conn=DriverManager.getConnection("jdbc:hive2://192.168.33.2:10000/default", "vagrant", "");
	    Statement st=conn.createStatement();
	    String tableName="test_small";//表名
	    ResultSet rs=st.executeQuery("select  count(1) from "+tableName+" ");//求平均数,会转成MapReduce作业运行
	    //ResultSet rs=st.executeQuery("select  * from "+tableName+" ");//查询所有,直接运行
	    while(rs.next()){
	    	System.out.println(rs.getString(1)+"   ");
	    }
	    System.out.println("成功!");
	    st.close();
	    conn.close();
		
	}
	
	
	

}
